# Callback Manager
> A callback manager for javascript.

## Install

[![NPM](https://img.shields.io/npm/v/chialab-callback-manager.svg)](https://www.npmjs.com/package/chialab-callback-manager)
```
$ npm i chialab-callback-manager --save
```
[![Bower](https://img.shields.io/bower/v/chialab-callback-manager.svg)](https://github.com/chialab/callback-manager-js)
```
$ bower i chialab-callback-manager --save
```

## Example
Global use:
```js
var myObj = {};
var cm = new CallbackManager();
// bind callback
cm.on(myObj, 'custom-event', function() {
    // this refers to `myObj`
    console.log(this);
});
// trigger callback
cm.trigger(myObj, 'custom-event');

```
Mixin use:
```js
// create a class
class Model extends CallbackManager.mixin() {
    constructor() {
        this.id = Date.now();
    }
    alert() {
        alert('Hello!')
    }
}
var model = new Model();
// bind callback
model.on('custom-event', function() {
    // this refers to `model`
    this.alert();
});
// trigger callback
model.trigger('custom-event');

```

## Dev

[![Chialab es6-workflow](https://img.shields.io/badge/project-es6--workflow-lightgrey.svg)](https://github.com/Chialab/es6-workflow)
[![Travis](https://img.shields.io/travis/Chialab/callback-manager-js.svg?maxAge=2592000)](https://travis-ci.org/Chialab/callback-manager-js)
[![Code coverage](https://codecov.io/gh/Chialab/callback-manager-js/branch/master/graph/badge.svg)](https://codecov.io/gh/Chialab/callback-manager-js)

![Sauce Test Status](https://saucelabs.com/browser-matrix/chialab.svg)
