## Classes

<dl>
<dt><a href="#CallbackManager">CallbackManager</a></dt>
<dd><p>An instantiable Callback Manager for javascript.</p>
</dd>
</dl>

## Mixins

<dl>
<dt><a href="#Mixin">`Mixin`</a></dt>
<dd></dd>
</dl>

## Members

<dl>
<dt><a href="#on">`on`</a> ⇒ <code>function</code></dt>
<dd><p>Add a callbacks for the specified trigger.</p>
</dd>
<dt><a href="#off">`off`</a></dt>
<dd><p>Remove all listeners.</p>
</dd>
<dt><a href="#trigger">`trigger`</a> ⇒ <code>Promise</code></dt>
<dd><p>Trigger a callback.</p>
</dd>
</dl>

<a name="CallbackManager"></a>

## CallbackManager
An instantiable Callback Manager for javascript.

**Kind**: global class  
**Mixes**: <code>[Mixin](#Mixin)</code>  
<a name="Mixin"></a>

## `Mixin`
**Kind**: global mixin  
<a name="on"></a>

## `on` ⇒ <code>function</code>
Add a callbacks for the specified trigger.

**Kind**: global variable  
**Returns**: <code>function</code> - Destroy created listener with this function  

| Param | Type | Description |
| --- | --- | --- |
| obj | <code>\*</code> | The object that triggers events |
| name | <code>String</code> | The event name |
| callback | <code>function</code> | The callback function |

<a name="off"></a>

## `off`
Remove all listeners.

**Kind**: global variable  

| Param | Type | Description |
| --- | --- | --- |
| obj | <code>\*</code> | The object that triggers events |
| name | <code>String</code> | Optional event name to reset |

<a name="trigger"></a>

## `trigger` ⇒ <code>Promise</code>
Trigger a callback.

**Kind**: global variable  
**Exec**: callback functions  

| Param | Type | Description |
| --- | --- | --- |
| obj | <code>\*</code> | The object that triggers events |
| name | <code>String</code> | Event name |
| ...args | <code>Array</code> | Arguments to pass to callback functions |

