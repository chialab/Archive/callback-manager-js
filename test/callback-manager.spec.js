import { CallbackManager } from '../src/callback-manager.js';

class Model extends CallbackManager.mixin() {
    get id() {
        return 'model';
    }
}

/* globals describe, before, after, beforeEach, it, assert */
describe('Unit: CallbackManager', () => {
    let myObj1 = {};
    let myObj2 = {};
    let myObj3 = {};
    let myObj4 = myObj1;
    let myObj5 = new Model();
    let cm = new CallbackManager();

    let check = {
        1: false,
        2: false,
        3: false,
        4: false,
    };

    function prepareFn(number) {
        return (...args) => {
            check[number] = args;
        };
    }

    let callback1 = prepareFn(1);
    let callback2 = prepareFn(2);
    let callback3 = prepareFn(3);
    let callback4 = prepareFn(4);

    describe('add listeners', () => {
        before((done) => {
            cm.on(myObj1, 'test', callback1);
            cm.on(myObj2, 'test', callback2);
            cm.on(myObj3, 'test', callback3);
            myObj5.on('test', callback4);
            Promise.all([
                cm.trigger(myObj4, 'test', 2),
                cm.trigger(myObj2, 'test', 'callback'),
                cm.trigger(myObj3, 'test', 1, 4, 2),
                myObj5.trigger('test', 2),
            ]).then(() => {
                done();
            });
        });

        after(() => {
            check = {
                1: false,
                2: false,
                3: false,
                4: false,
            };
        });

        it('simple callback', () => {
            assert.equal(check['1'][0], 2);
        });

        it('callback with one argument', () => {
            assert.equal(check['2'][0], 'callback');
        });

        it('callback with more arguments', () => {
            assert.equal(check['3'][0] + check['3'][1] + check['3'][2], 7);
        });

        it('mixin', () => {
            assert.equal(myObj5.id, 'model');
            assert.equal(check['4'][0], 2);
        });
    });

    describe('remove listeners', () => {
        before((done) => {
            cm.off(myObj1, 'test');
            cm.off(myObj2, 'test');
            cm.off(myObj3, 'test');
            myObj5.off();
            Promise.all([
                cm.trigger(myObj4, 'test', 2),
                cm.trigger(myObj2, 'test', 'callback'),
                cm.trigger(myObj3, 'test', 1, 4, 2),
                myObj5.trigger('test', 2),
            ]).then(() => {
                done();
            });
        });

        it('simple callback', () => {
            assert.equal(check['1'], false);
        });

        it('callback with one argument', () => {
            assert.equal(check['2'], false);
        });

        it('callback with more arguments', () => {
            assert.equal(check['3'], false);
        });

        it('mixin', () => {
            assert.equal(myObj5.id, 'model');
            assert.equal(check['4'], false);
        });
    });
});
